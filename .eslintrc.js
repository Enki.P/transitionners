const typescriptEslintRecommended = require('@typescript-eslint/eslint-plugin')
    .configs.recommended;

module.exports = {
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.ts', '.tsx', '.json'],
            },
        },
    },
    env: {
        browser: true,
        es6: true,
        jest: true,
        node: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'prettier',
    ],
    parserOptions: {
        sourceType: 'module',
    },
    plugins: [
        'simple-import-sort',
        'import',
        'prettier',
        'prefer-arrow',
    ],
    rules: {
        'padding-line-between-statements': [
            'error',
            {
                blankLine: 'always',
                prev: '*',
                next: ['return', 'if', 'for', 'switch'],
            },
            { blankLine: 'always', prev: 'class', next: '*' },
            { blankLine: 'always', prev: ['const', 'let'], next: '*' },
            {
                blankLine: 'any',
                prev: ['const', 'let'],
                next: ['const', 'let'],
            },
            { blankLine: 'always', prev: 'multiline-const', next: '*' },
        ],
        'prettier/prettier': [
            'error',
            {
                bracketSpacing: true,
                printWidth: 80,
                singleQuote: true,
                tabWidth: 4,
                trailingComma: 'all',
                jsxBracketSameLine: true,
                useTabs: false,
                endOfLine: 'auto',
            },
        ],
        'import/no-named-as-default': 'off',
        'import/no-named-as-default-member': 'off',
        'import/no-extraneous-dependencies': 'off',
        'import/first': 'error',
        'import/no-duplicates': 'error',
        'simple-import-sort/sort': 'error',
        'no-unused-vars': [
            'error',
            { vars: 'all', args: 'after-used', ignoreRestSiblings: true },
        ],
        'no-unused-expressions': 'off',
    },
    overrides: [
    ],
    globals: {
    },
};
