var Sprite = require("../../core/controllers/sprite.js");
var Animation = require("../../core/controllers/animation.js");
var PlayerInventory = require("./playerInventory.js");
var $ = require("jquery");


var playerDeplacementSpeed = 100

var Player = function(){
	this.deplacementSpeed = playerDeplacementSpeed;
	this.moveToCoords = false;
	this.movePath = false;
	this.manualMove = false;
	this.onDestination = function () { };
	this.action = null;
	this.speedX = 0;
	this.speedY = 0;

	var animationsArray = [
		new Animation("seed", 0, 4),
		new Animation('plow', 1, 4),
		new Animation('walk_side', 2, 4),
		new Animation('walk_down', 3, 4),
		new Animation('walk_up', 4, 4),
		new Animation('mulch', 5, 5),
		new Animation('weed', 6, 5)
	];

	var imgPath = require("../../../assets/personnage/spritesheet.png");

	this.sprite = new Sprite(300, 300, 60, 60, animationsArray, imgPath);
	this.sprite.zIndex = 1;
	this.sprite.origin = {x: 23, y: 50};

	this.spritesToRender = [this.sprite];

	this.forceAnimation = false;

	this.inventory = new PlayerInventory();
};


Player.prototype.giveItem = function(item) {
	if (!this.inventory.addItemToStorage(item)){
		console.warn("item not added : ", item);
	}
	item.player = this;
};

Player.prototype.giveItemToHotbar = function(item, pos) {
	if (!this.inventory.setHotbarItem(item, pos)){
		console.warn("item not added : ", item);
	}
	item.player = this;
};

Player.prototype._moveTo = function(coordsObj) {
	this.moveToCoords = {
		x: coordsObj.x,
		y: coordsObj.y
	}
	this.action = "walking";
};

Player.prototype.moveAlongPath = function(path, onDestination) {
	this.movePath = path;
	this._moveTo({ x: path[0][0], y: path[0][1] });

	if (typeof onDestination !== "function") return;
	let _this = this;
	this.onDestination = function () {
		onDestination();
		_this.onDestination = function () { };
	}
};

Player.prototype.standStill = function(){
	this.moveToCoords = false;
	this.sprite.curFrame = 0;
	this.sprite.srcX = 0;
	this.sprite.startAnimation("walk_down");
	this.sprite.flipped = false;
	this.sprite.stopAnimation();
}

Player.prototype.getCoords = function() {
	return {
		x: this.sprite.x,
		y: this.sprite.y
	}
};

Player.prototype.setPosition = function(pos, standStill){
	this.sprite.x = pos.x;
	this.sprite.y = pos.y;
	if (standStill) this.standStill();
}

Player.prototype.getPosition = function(){
	return {
		x: this.sprite.x,
		y: this.sprite.y,
	}
}

Player.prototype.isMoving = function() {
	var deplacementX = this.moveToCoords.x - this.sprite.x;
	var deplacementY = this.moveToCoords.y - this.sprite.y;
	if (!this.moveToCoords || Math.abs(deplacementX) < 1 && Math.abs(deplacementY) < 1){
		return false;
	}
	return true;
};

Player.prototype.beforeRendering = function (delta) {
	// calculate deplacement & animation
	if (delta > 1000) delta = 0.01;

	let specialEvent = function () { };
	if (this.moveToCoords && !this.manualMove){
		var deplacementX = this.moveToCoords.x - this.sprite.x;
		var deplacementY = this.moveToCoords.y - this.sprite.y;

		if (Math.abs(deplacementX) > 1 || Math.abs(deplacementY) > 1) {
			this.speedX = this.deplacementSpeed * deplacementX / Math.sqrt(Math.pow(deplacementX, 2) + Math.pow(deplacementY, 2));
			this.speedY = this.deplacementSpeed * deplacementY / Math.sqrt(Math.pow(deplacementX, 2) + Math.pow(deplacementY, 2));

		} else if (this.movePath && this.movePath.length > 1){
			this.movePath.shift();
			this._moveTo({x: this.movePath[0][0], y: this.movePath[0][1]});
			this.beforeRendering(delta);

		} else {
			this.speedX = 0;
			this.speedY = 0;
			this.standStill();
			this.action = null;
			if (this.movePath && this.movePath.length == 1){
				this.movePath.shift();
				specialEvent = this.onDestination;
			}
		}
	}

	this.sprite.x += this.speedX * delta / 1000;
	this.sprite.y += this.speedY * delta / 1000;

	if (this.speedX != 0 || this.speedY != 0) {
		if (Math.abs(this.speedY) > Math.abs(this.speedX)) {
			if (this.speedY < 0) {
				this.sprite.startAnimation("walk_up");
			} else {
				this.sprite.startAnimation("walk_down");
			}
		} else {
			this.sprite.startAnimation("walk_side");
		}
	}

	if (this.speedX < 0) {
		this.sprite.flipped = true;
	} else { this.sprite.flipped = false; }

	specialEvent()
	
};


Player.prototype.getActions = function (item, plot) {
	const actions = []
	for (let a in item.actions) {
		const action = item.actions[a]
		const player = this;
		if (this.canDo(plot, item, action)) {
			actions.push({
				name: action.name,
				animation: action.animation,
				action: function(){ player.doAction(plot, item, action) }
			})
		}
	}
	return actions
}

Player.prototype.canDo = function (plot, item, action) {
	// console.group("action check");
	// console.log(this);
	// console.log("do action", action);
	// console.log("on", plot);
	// console.log("with item", item);
	// console.groupEnd("action check");
	switch (action.name) {
		case "désherber":
			return (plot.surface === "field" && plot.state === "grass" && plot.fertilizer === null)
		case "labourer":
			return true;
		case "pailler":
			return (plot.surface === "field" && plot.fertilizer === null)
		case "semer":
			return (plot.surface === "field" && plot.state === "empty" && plot.getPlants().length < 6 * 4
						&& (item.currentPhase.name == "graine" || item.currentPhase.name == "graine germée"))
		case "planter":
			return (plot.surface === "field" && plot.state === "empty" && plot.getPlants().length < 6 * 4
						&& item.currentPhase.name == item.repiquagePhase);
		case "récolter":
			const plants = plot.getPlants();
			for (let p in plants) {
				const plant = plants[p]
				if (plant.currentPhase.name == plant.harvestPhase) return true
			}
			return false;
		case "récolter les graines":
			const plants2 = plot.getPlants();
			for (let p in plants2) {
				const plant2 = plants2[p]
				if (plant2.currentPhase.name == plant2.seedHarvestPhase) return true
			}
			return false;
		default:
			return true;
	}
}

Player.prototype.doAction = function(plot, item, action) {
	// console.group("action");
	// console.log(this);
	// console.log("do action", action);
	// console.log("on", plot);
	// console.log("with item", item);
	// console.groupEnd("action");
	var actionFunction = function () { };;
	var player = this;
	switch (action.name){
		case "désherber":
			actionFunction = function(){
				plot.setState("empty");
			};
			break;
		case "pailler":
			actionFunction = function(){
				plot.setFertilizer("mulch");
				player.inventory.changeItemQuantity(item, -1);
			};
			break;
		case "labourer":
			break;
		case "planter":
		case "semer":
			actionFunction = function(){
				var plant2 = item.divide(1);
				plot.putPlantInGround(plant2);
				item.refreshView();
			};
			break;
		case "récolter":
			actionFunction = function () {
				const plants = plot.getPlants();
				for (let p in plants) {
					const plant = plants[p]
					if (plant.currentPhase.name == plant.harvestPhase) {
						plot.clearSlot(plant.slot.x, plant.slot.y)
						plant.plantedInPlot = undefined;
						plant.slot = undefined;
						if (player.inventory.addItemInHotbar(plant) === false) {
							player.inventory.addItemToStorage(plant)
						}
					}
				}
			}
			break;
		case "récolter les graines":
			actionFunction = function () {
				const plants = plot.getPlants();
				for (let p in plants) {
					const plant = plants[p]
					if (plant.currentPhase.name == plant.seedHarvestPhase) {
						plot.clearSlot(plant.slot.x, plant.slot.y)
						plant.plantedInPlot = undefined;
						plant.slot = undefined;
						const seeds = plant.getSeeds();
						plant.destroy();
						if (player.inventory.addItemInHotbar(seeds) === false) {
							player.inventory.addItemToStorage(seeds)
						}
					}
				}
			}
			break;
		case "observer":
			plot.showObservationModal();
			break;
	}
	var player = this;
	if (action.animation) {
		this.sprite.startAnimation(action.animation, function () {
			actionFunction();
			player.standStill();
		});
	} else {
		actionFunction();
		player.standStill();
	}
	
};

module.exports = Player;