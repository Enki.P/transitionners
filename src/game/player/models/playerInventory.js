
var PlayerInventoryView = require("../views/playerInventoryView.js");
var PlayerInventoryController = require("../controllers/playerInventoryController.js");
const Player = require("./player.js");

//storage = inventaire complet
//backpack = slots d'accès rapide
function PlayerInventory(){
	this.coins = 200;
	this.storage = [];
	this.hotbar = [];
	/* chaque item dans la hotbar est de la forme :
	* {pos: x, item: <Item>}
	*/
	this.selectedItemPos = "shoes";
	this.hotbarSize = 10;

	this.view = new PlayerInventoryView(this);
	this.controller = new PlayerInventoryController(this);

	this.itemInHand = this.hotbar[0];

	this.view.refreshView()
}

PlayerInventory.prototype.setSelectedItemPos = function (pos) {
	this.selectedItemPos = pos;
	this.view.updateHotbarSelection();
}

PlayerInventory.prototype.getSelectedItem = function () {
	return this._getHotbarItem(this.selectedItemPos);
};

PlayerInventory.prototype.moveItemToHotbar = function(item, pos){
	if (pos == undefined){
		pos = this._getEmptyHotbarSlotPos();
	}
	for (i in this.storage){
		if (this.storage[i].uid == item.uid){
			var item = this.storage[i];
			const oldItemInPos = this.clearHotbarSlot(pos)
			this.setHotbarItem(item, pos)
			if (oldItemInPos !== true) this.addItemToStorage(oldItemInPos);
			this.storage.splice(i, 1);
			this.view.removeItemFromStorage(item);
			return true;
		}
	}
	return false;
}

PlayerInventory.prototype.moveItemToStorage = function (item) {
	const pos = this._getHotbarItemPosition(item)
	// console.log("position", pos)
	if (pos === false) return false
	if (!this.addItemToStorage(item)) return false;
	this.clearHotbarSlot(pos);
	return true;
}

PlayerInventory.prototype.moveItemInHotbar = function(item, newPos) {
	const oldPos = this._getHotbarItemPosition(item)

	const oldItemInPos = this.clearHotbarSlot(newPos)
	this.setHotbarItem(item, newPos);

	if (oldItemInPos !== true) {
		this.setHotbarItem(oldItemInPos, oldPos);
	} else {
		this.clearHotbarSlot(oldPos);
	}
	return true;
};

PlayerInventory.prototype.setHotbarItem = function (item, pos) {
	item.inventory = this;
	this.clearHotbarSlot(pos);
	this.hotbar.push({ "pos": pos, "item": item });
	this.view.setHotbarItem(item, pos)
	console.log("set hotbar item in pos", pos, item, this.hotbar)
	return true;
}

PlayerInventory.prototype.addItemInHotbar = function (item) {
	// check if item can be stacked with an item already in hotbar
	for (let i in this.hotbar) {
		const itemDest = this.hotbar[i].item
		if (itemDest.stackWith(item)) {
			this.destroyItem(item)
			return true;
		}
	}
	// get first empty slot, return false if there is no empty slot
	const emptyPos = this._getEmptyHotbarSlotPos();
	if (emptyPos) {
		this.setHotbarItem(item, emptyPos)
		item.inventory = this;
	} else {
		return false;
	}
}

PlayerInventory.prototype.clearHotbarSlot = function (pos) {
	// console.log("clear hotbar", pos)
	for (var i in this.hotbar) {
		if (this.hotbar[i].pos == pos) {
			const deletedItem = this.hotbar[i].item
			this.hotbar.splice(i, 1);
			this.view.clearHotbarSlot(pos);
			console.log("clear hotbar slot", pos, this.hotbar)
			if (this.selectedItemPos == pos) {
				this.setSelectedItemPos(this._getFirstNotEmptyHotbarSlotPos())
			}
			return deletedItem;
		}
	}
	return true;
}

PlayerInventory.prototype.addItemToStorage = function(item) {
	for (i in this.storage){
		const itemDest = this.storage[i]
		if (itemDest.stackWith(item)) {
			this.destroyItem(item)
			return true;
		}
	}
	this.storage.push(item);
	this.view.addItemInStorage(item);
	item.inventory = this;
	return true;
}

PlayerInventory.prototype.removeItemFromStorage = function(item) {
	for (i in this.storage){
		if (this.storage[i].uid == item.uid){
			this.storage.splice(i, 1);
			this.view.removeItemFromStorage(item);
			return true;
		}
	}
	return false;
};

PlayerInventory.prototype.changeItemQuantity = function(item, quantityToAdd){
	this.setItemQuantity(item, item.quantity + quantityToAdd)
}

PlayerInventory.prototype.setItemQuantity = function(item, quantity){
	item.quantity = quantity;
	if (item.quantity <= 0){
		this.destroyItem(item);
	}
	item.refreshView();
}

PlayerInventory.prototype.destroyItem = function (item) {
	this.removeItemFromStorage(item);
	this.clearHotbarSlot(this._getHotbarItemPosition(item));
	item.destroy();
}


PlayerInventory.prototype._getHotbarItem = function(pos) {
	for (var i in this.hotbar){
		var bpItem = this.hotbar[i];
		if (bpItem.pos == pos){
			return bpItem.item;
		}
	}
	return false;
};

PlayerInventory.prototype._getHotbarItemIndex = function(pos) {
	for (var i in this.hotbar) {
		var bpItem = this.hotbar[i];
		if (bpItem.pos == pos) {
			return i;
		}
	}
	return false;
};

PlayerInventory.prototype._getHotbarItemPosition = function(item) {
	for (var i in this.hotbar){
		var bpItem = this.hotbar[i];
		if (bpItem.item.uid == item.uid){
			return bpItem.pos;
		}
	}
	return false;
};

PlayerInventory.prototype._getEmptyHotbarSlotPos = function() {
	for (var pos = 1; pos <= this.hotbarSize -1; pos++){
		var itemDest = this._getHotbarItem(pos);
		if (itemDest === false){
			return pos;
		}
	}
	return false;
};

PlayerInventory.prototype._getFirstNotEmptyHotbarSlotPos = function () {
	let notEmptyPos = "shoes"
	for (let i in this.hotbar) {
		const pos = this.hotbar[i].pos
		if (!!parseInt(pos) && (pos < notEmptyPos || !parseInt(notEmptyPos) ) ) notEmptyPos = pos
	}
	return notEmptyPos;
};

module.exports = PlayerInventory;