const $ = require("jquery");
require( "jquery-ui/ui/widgets/draggable" );
require( "jquery-ui/ui/widgets/droppable" );

const Selector = require('../../gui/controllers/selector.js');
const gui = require("../../gui/controllers/gui.js");
const PlayerInventory = require("../models/playerInventory.js");


let PlayerInventoryView = function(playerInventory){
	this.inventory = playerInventory;

	this.storageFilters = [];
	const _this = this;
	this.storageFilters.push(new Selector({
		container: $('#inventory-menu .filter[filter-type="category"]'),
		data: [
			{
				"value": "all",
				"text": "Tous",
				"default": true
			},
			{
				"value": "plant",
				"text": "Plantes"
			},
			{
				"value": "tool",
				"text": "Outils"
			},
			{
				"value": "structure",
				"text": "Structures"
			}
		], 
		onChange: function(){
			_this.applyFilters();
		}
	}));
}

PlayerInventoryView.prototype.refreshView = function() {
	$('#inventory-content').empty();
	this.clearHotbar();
	var storage = this.inventory.storage;
	storage.sort(function(a, b) {
		var textA = a.name.toUpperCase();
		var textB = b.name.toUpperCase();
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
	});
	for (var i in storage){
		this.addItemInStorage(storage[i]);
	}

	var hotbar = this.inventory.hotbar;
	for (var i in hotbar){
		var hotbarSlot = hotbar[i];
		this.setHotbarItem(hotbarSlot.item, hotbarSlot.pos);
	}

	this.updateHotbarSelection()
};

PlayerInventoryView.prototype.clearHotbar = function() {
	$("#hotbar .slot").each(function(){
		$(this).data("item", undefined);
		$(this).attr("item-uid", undefined);
		$(this).find('.item-view').remove();
	})
};

PlayerInventoryView.prototype.addItemInStorage = function(item, addInBottom) {
	if (addInBottom === undefined) addInBottom = true;
	var $item = $('<div class="panel slot slot-small panel-small storage-item"></div>');
	gui.addPanelBackground($item);
	
	if (addInBottom){
		$('#inventory-content').append($item);
	} else {
		$('#inventory-content').prepend($item);
	}

	this.applyFilters();
	item.render($item);
	this.inventory.controller.attachItemEvents(item, $item);
};

PlayerInventoryView.prototype.removeItemFromStorage = function(item) {
	$('#inventory-content .storage-item[item-uid=' + item.uid + ']').remove();
};

PlayerInventoryView.prototype.setHotbarItem = function(item, pos) {
	var $item = $('#hotbar .slot[slot-id="'+parseInt(pos)+'"]');
	item.render($item);
	this.inventory.controller.attachItemEvents(item, $item);
};

PlayerInventoryView.prototype.clearHotbarSlot = function(pos) {
	var $slot = $('#hotbar .slot[slot-id=' + pos + ']');
	$slot.data("item", "");
	$slot.attr("item-uid", "");
	$slot.find(".item-view").remove();
};


PlayerInventoryView.prototype.updateStorageSelection = function () {
	$('#inventory-content .slot').each(function () {
		$(this).removeClass("selected")
	})
}

PlayerInventoryView.prototype.updateHotbarSelection = function () {
	$('#hotbar .slot').each(function ($slot) {
		$(this).removeClass("selected")
	})
	const selected = this.inventory.selectedItemPos
	$('#hotbar .slot[slot-id="' + selected + '"]').addClass("selected")
}

PlayerInventoryView.prototype.showItemModal = function(item, $item) {
	var inventory = this.inventory;
	var isInStockage = true;
	if ($item.parents('#hotbar').length > 0){
		isInStockage = false;
	}

	var btnText = isInStockage ? "Prendre" : "Stocker";
	var btnAction = function(){
		if (isInStockage){
			if (inventory.moveItemToHotbar(item)){
				gui.hideItemModal();
			} else {
				console.log("error while trying to move item from storage to hotbar", item)
			}
		} else {
			if (inventory.moveItemToStorage(item)){
				gui.hideItemModal();
			} else {
				console.log("error while trying to move item from hotbar to storage", item)
			}
		}
	}
	gui.showItemModal(item, $item, btnText, btnAction);
};

PlayerInventoryView.prototype.applyFilters = function() {
	for (var f in this.storageFilters){
		var selector = this.storageFilters[f];
		var filter = selector.getVal();
		if (filter == "all") {
			$('#inventory-content .storage-item').each(function(){
				$(this).show();
			});
			continue;
		};

		$('#inventory-content .storage-item').each(function(){
			$(this).show();
			var item = $(this).data("item");
			switch (selector.filterType){
				case "category":
				default:
					if (item.itemType != filter){
						$(this).hide();
					}
					break;
			}
		});
	}
	
};


module.exports = PlayerInventoryView;