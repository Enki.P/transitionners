var $ = require("jquery");
require( "jquery-ui/ui/widgets/draggable" );
require( "jquery-ui/ui/widgets/droppable" );


function PlayerInventoryController(playerInventory){
	this.inventory = playerInventory;

	var _this = this;
	var $hotbarSlots = $('#hotbar .slot');

	var alreadyClicked = false;
	$hotbarSlots.on("mousedown", function(){
		if (! $('#sidePanel').hasClass("collapsed")) return;

		_this.onHotbarSlotClick($(this));
	});
}

PlayerInventoryController.prototype.onHotbarSlotClick = function($slot) {
	const item = this.inventory._getHotbarItem($slot.attr("slot-id"))
	if (item || $slot.parent().hasClass("static-slots")) {
		this.inventory.setSelectedItemPos($slot.attr("slot-id"))
	}
};

PlayerInventoryController.prototype.attachItemEvents = function(item, $item) {
	var inventory = this.inventory;
	var isInStockage = true;
	if ($item.parents('#hotbar').length > 0){
		isInStockage = false;
	}

	// console.log({ hotbar: this.inventory.hotbar, storage: this.inventory.storage })

	var _this = this;
	var $itemView = $item.find(".item-view");
	$itemView.draggable({
		appendTo: "#GUI",
		// helper: "clone",
		revert: true,
		revertDuration: 200,
		start: function(){
			// $itemView.hide();
			$('#inventory-content').css("background", "#a4ff9957");
			$('#hotbar > .slot').append('<div class="droppable-bg"></div>');
			if (!isInStockage){
				$('#inventory-content').droppable({
					tolerance: "pointer",
					drop: function (event, ui) {
						inventory.moveItemToStorage(item);
						$('#item-modal').addClass("hidden");
					}
				});
			}
			$('#hotbar > .slot .droppable-bg').droppable({
				tolerance: "pointer",
				drop: function(event, ui){
					$itemView.draggable( "option", "revert", false );
					var pos = $(this).parent().attr("slot-id");
					if (isInStockage){
						inventory.moveItemToHotbar(item, pos);
					} else {
						inventory.moveItemInHotbar(item, pos);
					}
					$('#item-modal').addClass("hidden");
				}
			});
		},
		stop: function(){
			$itemView.show();
			$('#inventory-content').css("background", "none");
			$('#hotbar > .slot .droppable-bg').remove();
		}
	});
	
	$itemView.on("click", function(){
		if ($('#sidePanel').hasClass("collapsed")) return;
		$item.addClass("selected");
		_this.inventory.view.showItemModal(item, $item);
	});
};



module.exports = PlayerInventoryController;
