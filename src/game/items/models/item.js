const $ = require("jquery");

function Item(itemType){
	this.name = "";
	this.quantity = 1;
	this.itemType = itemType;
	this.stackable = true;
}

Item.prototype.init = function(dateSystem, creationDate){
	this.uid = Date.now().toString(16) + (Math.floor(Math.random()*10000)).toString(16);

	this.date = dateSystem;

	if (this.itemType === "plant"){
		this.updateWithTime();
	}
	if (creationDate === undefined){
		this.creationDate = this.date.date.getTime();
	} else {
		this.creationDate = creationDate;
	}
}

Item.prototype.destroy = function () {
	console.log("destroy", this.uid)
	var date = this.date;
	var newObjectsToUpdate = [];
	for (var o in date.objectsToUpdate){
		var item = date.objectsToUpdate[o];
		if (item.uid != this.uid) newObjectsToUpdate.push(item);
	}
	date.objectsToUpdate = newObjectsToUpdate;
};

Item.prototype.updateWithTime = function(){
	for (var o in this.date.objectsToUpdate){
		if (this.date.objectsToUpdate[o] == this.uid) return false;
	}
	this.date.objectsToUpdate.push(this);
	return true;
}

Item.prototype.stackWith = function (item) {
	if (this.name != item.name) return false;
	this.quantity += item.quantity;
	item.destroy()
	this.refreshView()
	return true;
}


Item.prototype.getIcon = function() {
	return require("../../../assets/items/" + this.itemType + "/" + this.name + ".png");
};


Item.prototype.getDescriptionFields = function () {
	return {
		name: this.name.capitalize()
	};
}

Item.prototype.refreshView = function () {
	var $item = $('.slot[item-uid="' + this.uid + '"]');
	this.render($item);
}

Item.prototype.render = function ($item) {
	$item.data("item", this);
	$item.attr("item-uid", this.uid);

	var $itemView = $item.find(".item-view");
	if ($itemView.length == 0) {
		$itemView = '<div class="item-view">';
		$itemView += '<img src="' + this.getIcon() + '"/>';
		if (this.stackable === true) {
			$itemView += '<div class="item-quantity">' + this.quantity + '</div>';
		}
		$itemView += "</div>";
		$item.append($itemView);
	} else {
		$itemView.find("img").attr("src", this.getIcon())
		if (this.stackable === true) {
			$itemView.find(".item-quantity").text(this.quantity)
		}
	}
}



module.exports = Item;