var Item = require("./item.js");

function Structure(name){

	this.name = name;
	this.itemType = "structure";

}

Structure.prototype = new Item("structure");

module.exports = Structure;