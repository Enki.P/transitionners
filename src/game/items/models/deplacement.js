var Item = require("./item.js");

function Deplacement(name, speed){
	this.name = name;
	this.speed = speed; 
}

Deplacement.prototype = new Item("deplacement");

module.exports = Deplacement;