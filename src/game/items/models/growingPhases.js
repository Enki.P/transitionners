
function Phase(name, canBeItem, duration, conditions){
	this.name = name;
	this.canBeItem = canBeItem;
	this.duration = duration;
	this.conditions = conditions;
}


function GrowingPhases(){
	this.phases = [];
}

GrowingPhases.prototype.addPhase = function(name, canBeItem, duration, conditions) {
	this.phases.push(new Phase(name, canBeItem, duration, conditions));
};

GrowingPhases.prototype.getPhaseByName = function(name) {
	for (var p in this.phases){
		if (this.phases[p].name == name) return this.phases[p];
	}
};

GrowingPhases.prototype.getPhaseByStep = function(step) {
	return this.phases[step];
};

GrowingPhases.prototype.getPhaseStep = function(name) {
	for (var p in this.phases){
		if (this.phases[p].name == name) return parseInt(p);
	}
};

GrowingPhases.prototype.getPhase = function(nameOrStep){
	if (typeof nameOrStep == "number"){
		return this.getPhaseByStep(nameOrStep);
	} else if (typeof nameOrStep == "string"){
		return this.getPhaseByName(nameOrStep);
	} else {
		return false;
	}
}

GrowingPhases.prototype.getNextPhase = function (name) {
	let phase = this.getPhaseByStep(this.getPhaseStep(name) + 1);
	if (phase) return phase
	return this.getPhaseByName(name)
};

module.exports = GrowingPhases;
