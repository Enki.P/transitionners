var Item = require("./item.js");
var GrowingPhases = require("./GrowingPhases.js");
var Sprite = require("../../core/controllers/sprite.js");
var Animation = require("../../core/controllers/animation.js");

function Plant(name,
				seedingStart, seedingEnd,
				harvestPhase, harvestAmount, seedHarvestAmount,
				repiquagePhase, dieOnHarvest, lifeDuration, 
				spriteInfos, description, phases){
	this.name = name;
	this.description = description;
	this.seedingStart = seedingStart;
	this.seedingEnd = seedingEnd;
	this.harvestPhase = harvestPhase;
	this.harvestAmount = harvestAmount;
	this.seedHarvestAmount = seedHarvestAmount;
	this.seedHarvestPhase = "fruit mature";
	this.repiquagePhase = repiquagePhase;
	this.dieOnHarvest = dieOnHarvest;
	this.lifeDuration = lifeDuration;
	this.spriteInfos = spriteInfos;
	this.itemType = "plant";
	this.actions = [
		{
			name: "semer",
			animation: "seed"
		},
		{
			name: "planter",
			animation: "seed"
		}
	]

	this.phases = new GrowingPhases();
	for (var p in phases){
		var phase = phases[p];
		this.phases.addPhase(phase.name, phase.canBeItem, phase.duration, phase.conditions);
	}

	this.currentPhase = this.phases.getPhaseByStep(0);

	var height = this.spriteInfos.height;
	var width = this.spriteInfos.width;
	var src = require("../../../assets/items/plant/"+ this.name.toLowerCase() +"/"+ this.name.toLowerCase() +"_spriteSheet.png");
	var animations = [
		new Animation("graine", 0, 1),
		new Animation("graine germée", 1, 1),
		new Animation("petite pousse", 2, 1),
		new Animation("moyenne pousse", 3, 1),
		new Animation("grande pousse", 4, 1),
		new Animation("jeune plante", 5, 1),
		new Animation("plante mature", 6, 1),
		new Animation("bourgeons", 7, 1),
		new Animation("fleurs", 8, 1),
		new Animation("jeune fruit", 9, 1),
		new Animation("fruit mature", 10, 1),
	];
	this.sprite = new Sprite(width * 10, height, width, height, animations, src);
	this.sprite.zIndex = 1;
	this.sprite.origin = this.spriteInfos.origin;
}

Plant.prototype = new Item("plant");

Plant.prototype.divide = function(quantity) {
	if (quantity >= this.quantity) return false;
	const plant2 = this.clone()
	plant2.quantity = quantity;
	this.quantity -= quantity;
	return plant2;
};

Plant.prototype.getSeeds = function () {
	const plant2 = this.clone()
	plant2.quantity = Math.floor(this.seedHarvestAmount[0] + (this.seedHarvestAmount[1] - this.seedHarvestAmount[0] + 1)*Math.random());
	plant2.currentPhase = this.phases.getPhaseByName("graine")
	plant2.phaseStart = this.date.date.getTime();
	return plant2;
};

Plant.prototype.clone = function () {
	var phases = [];
	for (var p in this.phases.phases) {
		var phase = this.phases.phases[p];
		phases.push({
			name: phase.name,
			canBeItem: phase.canBeItem,
			duration: phase.duration,
			conditions: phase.conditions
		});
	}

	var plant2 = new Plant(
		this.name,
		this.seedingStart,
		this.seedingEnd,
		this.harvestPhase,
		this.harvestAmount,
		this.seedHarvestAmount,
		this.repiquagePhase,
		this.dieOnHarvest,
		this.lifeDuration,
		this.spriteInfos,
		this.description,
		phases);

	if (this.stackable === false) plant2.stackable = false;
	if (this.default === true) plant2.default = true;
	plant2.init(this.date);

	plant2.quantity = this.quantity;
	plant2.currentPhase = this.currentPhase;
	plant2.phaseStart = this.phaseStart;
	return plant2;
}

Plant.prototype.stackWith = function (item) {
	if (this.name != item.name) return false;
	if (this.currentPhase.name != item.currentPhase.name) return false;

	const hoursSinceLastPhase1 = this.date.getHoursSinceDate(this.phaseStart);
	const hoursSinceLastPhase2 = this.date.getHoursSinceDate(item.phaseStart);
	const averageDuration = (hoursSinceLastPhase1 * this.quantity + hoursSinceLastPhase2 * item.quantity) / (item.quantity + this.quantity)

	this.quantity += item.quantity;
	item.destroy()
	this.refreshView()
	return true;
}

Plant.prototype.getIcon = function() {
	let icon;
	try {
		icon = require("../../../assets/items/plant/" + this.name.toLowerCase() + "/" + this.name.toLowerCase() + "_" + this.currentPhase.name.replace(/ /g, "_") + ".png");
	} catch (e) {
		icon = require("../../../assets/items/plant/" + this.name.toLowerCase() + "/" + this.name.toLowerCase() + "_" + this.harvestPhase.replace(/ /g, "_") + ".png");
	}
	return icon
};

Plant.prototype.update = function() {
	if (! this.phaseStart){
		this.phaseStart = this.date.date.getTime();
		return;
	}
	var daysSinceLastPhase = Math.floor(this.date.getHoursSinceDate(this.phaseStart) / 24);
	var phaseDuration = this.currentPhase.duration;
	if (this.currentPhase.name == "graine" && this.plantedInPlot != null) phaseDuration = 5
	if (daysSinceLastPhase >= phaseDuration){
		this.nextPhase();
		this.sprite.setAnimation(this.currentPhase.name);
		if (this.inventory) {
			if (this.currentPhase.name != "graine"
				&& this.currentPhase.name != "graine germée"
				&& this.currentPhase.name != this.harvestPhase
				&& this.currentPhase.name != this.repiquagePhase)
			{
				this.inventory.destroyItem(this)
				this.destroy()
			} else {
				this.refreshView()
			}
		}
	}
	console.groupCollapsed(this.name + " : " + daysSinceLastPhase +"/"+phaseDuration);
	console.log(this);
	console.groupEnd();
};

Plant.prototype.nextPhase = function(){
	var newPhase = this.phases.getNextPhase(this.currentPhase.name);
	if (newPhase.name != this.currentPhase.name) {
		this.phaseStart = this.date.date.getTime();
	}
	this.currentPhase = newPhase;
}

Plant.prototype.getDescriptionFields = function () {
	const seedingStartDate = new Date()
	seedingStartDate.setDate(this.seedingStart[1])
	seedingStartDate.setMonth(this.seedingStart[0])
	const seedingEndDate = new Date()
	seedingEndDate.setDate(this.seedingEnd[1])
	seedingEndDate.setMonth(this.seedingEnd[0])
	console.log(seedingEndDate)
	let repiquage, harvest;
	if (this.repiquagePhase == null) {
		repiquage = "Pas de repiquage";
		const harvestPhaseStep = this.phases.getPhaseStep(this.harvestPhase);
		let harvestDayStart = 0;
		for (var p = 1; p < harvestPhaseStep; p++) {
			harvestDayStart += this.phases.getPhaseByStep(p).duration;
		}
		const harvestDayEnd = harvestDayStart + this.phases.getPhaseByStep(harvestPhaseStep).duration;

		const harvestStartDate = new Date(seedingStartDate.getTime() + harvestDayStart * 24 * 60 * 60 * 1000)
		const harvestEndDate = new Date(seedingEndDate.getTime() + harvestDayEnd * 24 * 60 * 60 * 1000)

		const harvestStart = this.date.getPartOfMonth(harvestStartDate);
		const harvestEnd = this.date.getPartOfMonth(harvestEndDate);
		harvest = harvestStart + " à " + harvestEnd;

	} else {
		var repiquagePhaseStep = this.phases.getPhaseStep(this.repiquagePhase);
		var daysMin = 0;
		for (var p = 1; p < repiquagePhaseStep; p++) {
			daysMin += this.phases.getPhaseByStep(p).duration;
		}
		var daysMax = daysMin + this.phases.getPhaseByStep(repiquagePhaseStep).duration;
		repiquage = Math.floor(daysMin / 7) + " à " + Math.ceil(daysMax / 7) + " semaines après semis";
		var harvestPhaseStep = this.phases.getPhaseStep(this.harvestPhase);
		var harvestDayStart = 0;
		for (var p = repiquagePhaseStep; p < harvestPhaseStep; p++) {
			harvestDayStart += this.phases.getPhaseByStep(p).duration;
		}
		var harvestDayEnd = harvestDayStart + this.phases.getPhaseByStep(harvestPhaseStep).duration;
		harvest = Math.floor(harvestDayStart / 7) + " à " + Math.ceil(harvestDayEnd / 7) + " semaines après repiquage";
	}

	return {
		name: this.name.capitalize() + " - " + this.currentPhase.name,
		description: this.description,
		seedingStart: this.date.getPartOfMonth(seedingStartDate),
		seedingEnd: this.date.getPartOfMonth(seedingEndDate),
		repiquage: repiquage,
		harvest: harvest
	};
}



module.exports = Plant;