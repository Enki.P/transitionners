var Item = require("./item.js");

function Tool(name, disposable, actions){
	this.name = name;
	this.disposable = disposable;
	this.actions = actions;
	this.itemType = "tool";
}

Tool.prototype = new Item("tool");

module.exports = Tool;