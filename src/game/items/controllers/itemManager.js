var itemsJson = require("../data/items.json");
var Plant = require("../models/plant.js");
var Structure = require("../models/structure.js");
var Tool = require("../models/tool.js");
var Deplacement = require('../models/deplacement.js');


function ItemManager(dateSystem){
	this.date = dateSystem;
}

ItemManager.prototype.createPlant = function(name) {
	return this._createItem(name, "plant");
};

ItemManager.prototype.createDeplacement = function(name) {
	return this._createItem(name, "deplacement");
};

ItemManager.prototype.createTool = function(name) {
	return this._createItem(name, "tool");
};

ItemManager.prototype.createStructure = function(name) {
	return this._createItem(name, "structure");
};

ItemManager.prototype._createItem = function(name, itemType) {
	var items = itemsJson[itemType];
	for (var i in items){
		var item = items[i];
		if (item.name != name) continue;

		var newItem;
		switch (itemType){
			case "deplacement":
				newItem = new Deplacement(item.name, item.speed);
				break;
			case "plant":
				newItem = new Plant(
					item.name,
					item.seedingStart,
					item.seedingEnd, 
					item.harvestPhase, 
					item.harvestAmount,
					item.seedHarvestAmount,
					item.repiquagePhase, 
					item.dieOnHarvest, 
					item.lifeDuration,
					item.spriteInfos,
					item.description,
					item.phases
				);
				break;
			case "tool":
				newItem = new Tool(item.name, item.disposable, item.actions);
				break;
			case "structure":
				newItem = new Structure(item.name);
				break;
		};
		if (item.stackable === false) newItem.stackable = false;
		if (item.default === true) newItem.default = true;
		newItem.init(this.date);
		return newItem;
	}
};

module.exports = ItemManager;
