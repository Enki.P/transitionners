require("../css/app.less");

require("./utils.js");
var Assets = require("./assets.js");
var GameModel = require("../models/gameModel.js");
var ItemsManager = require("../../items/controllers/itemManager.js");
var gui = require("../../gui/controllers/gui.js");
var $ = require("jquery");

var App = function(){
	this.gameModel = new GameModel();
	var itemsManager = new ItemsManager(this.gameModel.date);
	var player = this.gameModel.player;
	var shoes = itemsManager.createDeplacement("chaussures");
	player.giveItemToHotbar(shoes, "shoes");
	var gloves = itemsManager.createTool("gants");
	player.giveItemToHotbar(gloves, "hands");
	var gloves = itemsManager.createTool("loupe");
	player.giveItemToHotbar(gloves, "observe");
	
	var laitue = itemsManager.createPlant("laitue");
	laitue.quantity = 10;
	player.giveItemToHotbar(laitue, 1);
	var mulch = itemsManager.createTool("paillage");
	mulch.quantity = 10;
	player.giveItem(mulch, 2);
	// console.log(this.gameModel);
	// console.log(player.inventory);
	// this.gameModel.appView.refreshStorage();

	this.avgFps = 60;
}

App.prototype.start = function () {
	this.lastFrameDate = Date.now();
	this.main();
}

App.prototype.main = function() {
	this.now = Date.now();
	this.delta = this.now - this.lastFrameDate;
	this.lastFrameDate = this.now;
	if (this.delta === 0) this.delta = 1
	this.avgFps = (this.avgFps * 49 + 1/(this.delta/1000)) / 50;
	$('#fps').text(Math.round(this.avgFps));

	this.gameModel.date.calculateNewDate(this.delta)
	
	this.gameModel.canvas.clear();
	this.gameModel.canvas.renderObjects(this.gameModel.objectsToRender, this.delta);
	
	this.gameModel.canvas.onRender(this.delta);

	var _this = this;
	window.requestAnimationFrame(function(){_this.main()});
};

module.exports = App;