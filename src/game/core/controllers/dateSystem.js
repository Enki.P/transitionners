var calendar = require("../data/calendar.json");
var DateView = require("../views/dateView.js");

var Date2 = function () {
	this.calendar = calendar;
	this.date = new Date("01/01/2001 08:00")

	this.season = 1;

	// X ms IRL = 1h ingame
	this.timeFactor = 1;
	this.userFactor = 1;
	this.baseTimeFactor = 720; // 5s IRL = 3600s ingame

	this.realTimeSinceLastTick = 0;
	this.timeBetweenTicks = 500;

	this.objectsToUpdate = [];

	this.view = new DateView(this);
	this.updateTimeFactor();
}

Date2.prototype.setUserFactor = function (newUserFactor) {
	this.userFactor = newUserFactor
	this.updateTimeFactor();
}

Date2.prototype.updateTimeFactor = function () {
	this.timeSinceLastHour = 0;
	this.timeFactor = this.baseTimeFactor * this.userFactor
}

Date2.prototype.calculateNewDate = function (delta) {
	this.realTimeSinceLastTick += delta;
	this.date = new Date(this.date.getTime() + delta * this.timeFactor)
	this.view.renderDate();
	if (this.realTimeSinceLastTick > this.timeBetweenTicks) {
		this.realTimeSinceLastTick = 0;
		this.updateObjects();
	}
}


Date2.prototype.updateObjects = function() {
	console.group("updateObjects");
	const objectsToUpdate = []
	for (var o in this.objectsToUpdate) {
		objectsToUpdate.push(this.objectsToUpdate[o])
	}
	for (var o in objectsToUpdate){
		var item = objectsToUpdate[o];
		if (typeof item.update === "function"){
			item.update();
		} else {
			console.log(item);
		}
	}
	console.groupEnd("updateObjects");
};

Date2.prototype.getMonthPartFromDay = function(day){
	if (day <= 10) return "début ";
	if (day > 20) return "fin ";
	return "mi-";
}

Date2.prototype.getPartOfMonth = function (date) {
	return this.getMonthPartFromDay(date.getDate()) + date.toLocaleDateString("default", { month: "long" })
}

Date2.prototype.getHoursSinceDate = function (date) {
	const diffTime = this.date - date
	var diffHours = Math.floor(diffTime / (1000 * 60 * 60));
	return diffHours;
};

Date2.prototype.getSeason = function(date) {
	if (date === undefined) date = this.date;
	var seasonNumber = 3;
	for (var s=0; s<4; s++){
		if (date.getMonth() > this.calendar.seasons[s].from.month ||
			date.getMonth() == this.calendar.seasons[s].from.month && date.getDate() >= this.calendar.seasons[s].from.day){
			seasonNumber = s;
		}
	}
	return this.calendar.seasons[seasonNumber].name;
};



module.exports = Date2;