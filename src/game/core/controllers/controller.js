const $ = require("jquery");
const PF = require('pathfinding');
const gui = require("../../gui/controllers/gui.js");

var Controller = function(gameModel){
	this.gameModel = gameModel;
	this.mouseIsMoving = false;
	this.mouseIsDown = false;
	this.clickCoords = [];
	this.clicks = 0;
	this.playerMoveDirection = new Set();
	var _this = this;

	var $canvas = $(this.gameModel.canvas.$element);
	$canvas.on('mousedown', function(event){
		_this.mouseIsMoving = false;
		_this.mouseIsDown = true;
		_this.clickCoords = [event.clientX, event.clientY];
	});
	$canvas.on('mousemove', function(event){
		if (_this.mouseIsDown === false) return;
		const distance = Math.abs(event.originalEvent.x - _this.clickCoords[0]) + Math.abs(event.originalEvent.y - _this.clickCoords[1])
		if (distance < 100) return;
		_this.mouseIsMoving = true;
		_this.onMouseDrag(event);
	});
	$canvas.on('mouseup', function(event){
		_this.mouseIsDown = false;
		if (!_this.mouseIsMoving){
			_this.onMouseClick(event);
		}
		else if (_this.mouseIsMoving){
			//dragEnd
		}
		_this.mouseIsMoving = false;
	});
	$(document).on('keydown', function (event) {
		switch (event.which) {
			case 90:
			case 38:
				_this.playerMoveDirection.add("up");
				_this.playerMoveDirection.delete("down");
				break;
			case 83:
			case 40:
				_this.playerMoveDirection.add("down");
				_this.playerMoveDirection.delete("up");
				break;
			case 81:
			case 37:
				_this.playerMoveDirection.add("left");
				_this.playerMoveDirection.delete("right");
				break;
			case 68:
			case 39:
				_this.playerMoveDirection.add("right");
				_this.playerMoveDirection.delete("left");
				break;
		}
		_this.calculatePlayerMoveSpeed()
	});
	$(document).on('keyup', function (event) {
		switch (event.which) {
			case 90:
			case 38:
				_this.playerMoveDirection.delete("up");
				break;
			case 83:
			case 40:
				_this.playerMoveDirection.delete("down");
				break;
			case 81:
			case 37:
				_this.playerMoveDirection.delete("left");
				break;
			case 68:
			case 39:
				_this.playerMoveDirection.delete("right");
				break;
		}
		_this.calculatePlayerMoveSpeed()
	});

	$canvas.on('mousewheel DOMMouseScroll', function(event){
		// if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
		var canvas = _this.gameModel.canvas;
		canvas.cameraTarget = null;
		canvas.zooming(event.originalEvent.wheelDelta, canvas.getClickCoordinates(event));
	});
}


Controller.prototype.onMouseClick = function (e) {
	if (!$('#sidePanel').hasClass("collapsed")) return;
	var clickCoords = this.gameModel.canvas.getClickCoordinates(e);
	var player = this.gameModel.player;
	var canvas = this.gameModel.canvas;
	var selectedItem = player.inventory.getSelectedItem();
	if ((player.action === null || player.action === "walking") && selectedItem.itemType === "deplacement"){
		canvas.cameraMovementDisabled = false;
		this.movePlayer(clickCoords);
		gui.hideActionsModals();

	} else {
		gui.hideActionsModals();
		var clickedPlot = this.gameModel.terrain.getPlotFromClickCoords(clickCoords);
		if (!clickedPlot) return;

		var controller = this;

		const moveCoords = {
			x: clickedPlot.sprite.x + 0.4 * clickedPlot.sprite.width,
			y: clickedPlot.sprite.y + 0.6 * clickedPlot.sprite.height
		};
		const actions = player.getActions(selectedItem, clickedPlot)

		if (actions.length === 0) {
			console.log("no action")
			controller.movePlayer(moveCoords)
		} else if (actions.length === 1) {
			controller.movePlayer(moveCoords, actions[0].action);
		} else {
			gui.showActionsModal(clickedPlot, actions, function (action) {
				controller.movePlayer(moveCoords, action);
			}, function () {
				gui.hideActionsModals();
			});
		}
		
	}
};

Controller.prototype.onMouseDrag = function(e) {
	var canvas = this.gameModel.canvas;
	var x = canvas.viewX - e.originalEvent.movementX / canvas.zoom;
	var y = canvas.viewY - e.originalEvent.movementY / canvas.zoom;
	canvas.moveCameraTo({x:x, y:y});
	canvas.cameraTarget = null;
};


Controller.prototype.movePlayer = function(clickCoords, onDestination) {
	var player = this.gameModel.player;
	var terrain = this.gameModel.terrain;
	var fromCoords = player.getCoords();
	var deplacementData = {
		from: {
			exact : fromCoords,
			grid : terrain.getGridCoordsFromGameCoords(fromCoords)
		},
		to: {
			exact : clickCoords,
			grid : terrain.getGridCoordsFromGameCoords(clickCoords)
		}
	}
	if (! terrain.pfGrid.isWalkableAt(deplacementData.from.grid.x, deplacementData.from.grid.y)){
		// console.log("start point not walkable", deplacementData.from.grid.x, deplacementData.from.grid.y);
		return;
	}
	if (! terrain.pfGrid.isWalkableAt(deplacementData.to.grid.x, deplacementData.to.grid.y)){
		// console.log("end point not walkable", deplacementData.to.grid.x, deplacementData.to.grid.y);
		return;
	}
	var path = terrain.getPath(deplacementData);
	player.moveAlongPath(path, onDestination);
	this.gameModel.canvas.cameraTarget = player;
}

Controller.prototype.calculatePlayerMoveSpeed = function () {
	var player = this.gameModel.player;
	player.speedY = 0;
	player.speedX = 0;

	if (this.playerMoveDirection.has("up")) {
		if ([...this.playerMoveDirection].length == 1) {
			player.speedY = -player.deplacementSpeed
		} else {
			player.speedY = -player.deplacementSpeed / Math.sqrt(2)
		}
	}

	if (this.playerMoveDirection.has("down")) {
		if ([...this.playerMoveDirection].length == 1) {
			player.speedY = player.deplacementSpeed
		} else {
			player.speedY = player.deplacementSpeed / Math.sqrt(2)
		}
	}

	if (this.playerMoveDirection.has("left")) {
		if ([...this.playerMoveDirection].length == 1) {
			player.speedX = -player.deplacementSpeed
		} else {
			player.speedX = -player.deplacementSpeed / Math.sqrt(2)
		}
	}

	if (this.playerMoveDirection.has("right")) {
		if ([...this.playerMoveDirection].length == 1) {
			player.speedX = player.deplacementSpeed
		} else {
			player.speedX = player.deplacementSpeed / Math.sqrt(2)
		}
	}

	if ([...this.playerMoveDirection].length == 0) {
		player.manualMove = false
		player.speedX = 0
		player.speedY = 0
		player.standStill()
	} else {
		player.manualMove = true
	}
}



module.exports = Controller;