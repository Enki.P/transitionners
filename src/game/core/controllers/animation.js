
var Animation = function(name, row, frameCount){
	if (typeof name == "object" && frameCount == undefined && row == undefined){
		this.name = name.name;
		this.frameCount = name.frameCount;
		this.row = name.row;
	} else {
		this.name = name;
		this.frameCount = frameCount;
		this.row = row;
	}
}


module.exports = Animation;