var $ = require("jquery");



var Canvas = function(){
	this.$element = $("canvas#gameCanvas").attr("width", window.innerWidth).attr("height", window.innerHeight),
	this.ctx = this.$element.get(0).getContext("2d");
	this.ctx.imageSmoothingEnabled = false;
	this.viewX = 0;
	this.viewY = 0;
	this.zoom = 2;
	this.maxGameHeight = 1080;
	this.maxGameWidth = 1920;
	this.cameraTarget = null;
	this.cameraTargetCoords = {x: 0, y: 0};
	this.cameraMovementDisabled = false;

	var _this = this;
	$(window).resize(function(){
		_this.$element.attr("width", window.innerWidth).attr("height", window.innerHeight);
		_this.ctx.imageSmoothingEnabled = false;
		_this.zooming(1);
		_this.zooming(-1);
	});
}



Canvas.prototype.getWidth = function(){
	return this.$element.get(0).width;
}
Canvas.prototype.getHeight = function(){
	return this.$element.get(0).height;
}

Canvas.prototype.zooming = function(wheelDelta, pointerCoords){
	if (this.cameraMovementDisabled) return;
	var _this = this;
	if (pointerCoords == undefined) pointerCoords = {x: this.viewX, y: this.viewY};
	else pointerCoords = {x: pointerCoords.x - this.getWidth()/this.zoom/2, y: pointerCoords.y - this.getHeight()/this.zoom/2};
	var oldWidth = this.getWidth()/this.zoom;
	var oldHeight = this.getHeight()/this.zoom;
	if (wheelDelta > 0) {
		this.zoom = this.zoom * 1.1;
	}
	else {
		zoomDirection = -1;
		this.zoom = this.zoom / 1.1;
		this.zoom = Math.max(this.zoom, Math.max(this.getHeight()/this.maxGameHeight, this.getWidth()/this.maxGameWidth));
	}
	
	if (this.zoom%1 < 0.1 || this.zoom%1 > 0.9) this.zoom = Math.round(this.zoom);

	var newWidth = this.getWidth()/this.zoom;
	var newHeight = this.getHeight()/this.zoom;
	if (wheelDelta > 0) {
		this.moveCameraTo({
			x: (this.viewX + pointerCoords.x*0.1)/1.1 - 1/2 * (newWidth - oldWidth),
			y: (this.viewY + pointerCoords.y*0.1)/1.1 - 1/2 * (newHeight - oldHeight),
		});
	} else {
		this.moveCameraTo({
			x: (this.viewX*0.9 + (2*this.viewX-pointerCoords.x)*0.1) - 1/2 * (newWidth - oldWidth),
			y: (this.viewY*0.9 + (2*this.viewY-pointerCoords.y)*0.1) - 1/2 * (newHeight - oldHeight),
		});
	}
}

Canvas.prototype.moveCameraTo = function(coordsObj, animated){
	if (this.cameraMovementDisabled) return;
	if (!animated){
		this._moveCameraTo({
			x: Math.round(coordsObj.x),
			y: Math.round(coordsObj.y)
		});
		this.cameraTargetCoords = coordsObj;
		return;
	}

	this.cameraTargetCoords = coordsObj;
}

Canvas.prototype._moveCameraTo = function(coordsObj){
	if (this.cameraMovementDisabled) return;
	this.viewX = Math.max(0, coordsObj.x);
	this.viewY = Math.max(0, coordsObj.y);
	this.viewX = Math.min(this.viewX, this.maxGameWidth - this.getWidth()/this.zoom);
	this.viewY = Math.min(this.viewY, this.maxGameHeight - this.getHeight()/this.zoom);
}

Canvas.prototype.clear = function(){
	this.ctx.clearRect(0, 0, this.$element.get(0).width, this.$element.get(0).height);
}

Canvas.prototype.getClickWindowCoordinates = function(e){
	var x;
	var y;
	if (e.pageX || e.pageY) { 
		x = e.pageX;
		y = e.pageY;
	}
	else { 
		x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
		y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
	}
	x -= this.$element.get(0).offsetLeft;
	y -= this.$element.get(0).offsetTop;

	return {x:x, y:y};
}


Canvas.prototype.getClickCoordinates = function(e){
	var windowCoords = this.getClickWindowCoordinates(e);
	var x = this.viewX + windowCoords.x / this.zoom; 
	var y = this.viewY + windowCoords.y / this.zoom; 
	return {x:x, y:y};
}

Canvas.prototype.moveCameraToPlayer = function(player, toleranceX, toleranceY) {
	var _this = this;
	var x = player.getPosition().x - _this.getWidth()/_this.zoom/2;
	var y = player.getPosition().y - _this.getHeight()/_this.zoom/2;
	if (toleranceX == undefined) toleranceX = 0;
	if (toleranceY == undefined) toleranceY = 0;
	toleranceX = toleranceX /_this.zoom;
	toleranceY = toleranceY /_this.zoom;
	if (Math.abs(this.viewX - x) < toleranceX && Math.abs(this.viewY - y) < toleranceY) return;

	if (Math.abs(this.viewX - x) >= toleranceX){
		if (this.viewX - x > 0) x += toleranceX;
		else x -= toleranceX;
	} else x = this.viewX;

	if (Math.abs(this.viewY - y) >= toleranceY){
		if (this.viewY - y > 0) y += toleranceY;
		else y -= toleranceY;
	} else y = this.viewY;

	this.moveCameraTo({
		x: x,
		y: y
	}, true);
};

Canvas.prototype.renderObjects = function(objects, delta) {
	this.spritesToRender = [];
	for (var o in objects){
		var obj = objects[o];
		this._addSpritesToRender(obj, delta);
	}

	this.spritesToRender.sort(function (a, b) {
		if (a.zIndex > b.zIndex || a.zIndex == b.zIndex && a.y > b.y) return 1;
		return -1;
	});
	for (var s in this.spritesToRender){
		this.spritesToRender[s].draw(this, delta);
	}
	for (var o in objects){
		var obj = objects[o];
		if (obj.spritesToRender == null) continue;
		if (typeof obj.onRender == "function"){
			obj.onRender(delta);
		}
		if (! (obj.objectsToRender && obj.objectsToRender.length > 0) ) continue;
		for (var o in obj.objectsToRender){
			if (typeof obj.objectsToRender[o].onRender == "function"){
				obj.onRender(delta);
			}
		}
	}
}

Canvas.prototype._addSpritesToRender = function(obj, delta){
	if (obj.spritesToRender && obj.spritesToRender.length > 0){
		for (var s in obj.spritesToRender){
			this.spritesToRender.push(obj.spritesToRender[s]);
		}
	}
	if (typeof obj.beforeRendering == "function"){
		obj.beforeRendering(delta);
	}
	if (! (obj.objectsToRender && obj.objectsToRender.length > 0) ) return;
	for (var o in obj.objectsToRender){
		this._addSpritesToRender(obj.objectsToRender[o]);
	}
}

// Canvas.prototype.renderObject = function(obj, delta) {
// 	if (obj.spritesToRender == null){
// 		console.warn("no sprite to render for this object : ", obj);
// 		return;
// 	}
// 	if (typeof obj.beforeRendering == "function"){
// 		obj.beforeRendering(delta);
// 	}
// 	obj.spritesToRender.sort(function(a, b){return a.zIndex - b.zIndex});
// 	for (var s in obj.spritesToRender){
// 		obj.spritesToRender[s].draw(this, delta);
// 	}
// 	if (typeof obj.onRender == "function"){
// 		obj.onRender(delta);
// 	}
// };

Canvas.prototype.onRender = function(delta){
	if (this.cameraTarget){
		this.moveCameraToPlayer(this.cameraTarget, 200, 100);
	}

	if (this.cameraTargetCoords.x != this.viewX || this.cameraTargetCoords.y != this.viewY){
		var timeCoef = Math.min(1, delta/250);
		var diffxCoef = Math.min(1, Math.abs(this.viewX - this.cameraTargetCoords.x)/10);
		var diffyCoef = Math.min(1, Math.abs(this.viewY - this.cameraTargetCoords.y)/10);
		var moveCoefX = Math.min(1, timeCoef/diffxCoef);
		var moveCoefY = Math.min(1, timeCoef/diffyCoef);
		if (diffxCoef < 0.1 && diffyCoef < 0.1){
			this.moveCameraTo(this.cameraTargetCoords, false);
			return;
		}
		this._moveCameraTo({
			x: (1-moveCoefX)*this.viewX + moveCoefX*this.cameraTargetCoords.x,
			y: (1-moveCoefY)*this.viewY + moveCoefY*this.cameraTargetCoords.y
		});
	}
}



module.exports = Canvas;