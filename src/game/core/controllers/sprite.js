var $ = require("jquery");
var Animation = require("./animation.js");

var Sprite = function(sheetWidth, sheetHeight, spriteWidth, spriteHeight, animationsArray, imgPath){
	this.sheetWidth = sheetWidth;
	this.sheetHeight = sheetHeight;
	this.width = spriteWidth;
	this.height = spriteHeight;

	this.img = new Image();
	this.img.src = imgPath;
	this.img.onload = function () {
		if (this.onLoad != null){
			this.onLoad();
		}
	};

	this.animations = [];
	if (animationsArray){
		for (var a in animationsArray){
			this.animations.push(new Animation(animationsArray[a]));
		}
	} else {
		this.animations = [new Animation("sprite", 1, 0)];
	}

	this.animated = false;
	this.curAnimation = this.animations[0];
	this.curFrame = 0;
	this.onAnimationEnd = function(){};

	this.x = 0;
	this.y = 0;
	this.srcX = 0;
	this.srcY = 0;
	this.animationSpeed = 7;
	this.delta = 0;
	this.flipped = false;
	this.origin = {x:0, y:0};
}

Sprite.prototype.moveTo = function(x, y) {
	this.x = x;
	this.y = y;
}

Sprite.prototype.nextFrame = function() {
	this.curFrame++;
	if (this.curFrame >= this.curAnimation.frameCount){
		if (typeof this.onAnimationEnd === 'function'){
			this.onAnimationEnd();
			this.onAnimationEnd = null;
		}
		this.curFrame = 0;
	}
	// this.curFrame = ++this.curFrame % this.curAnimation.frameCount;
	this.srcX = this.curFrame * this.width; 
	this.srcY = this.curAnimation.row * this.height;
};

Sprite.prototype.setFrame = function(frame){
	this.curFrame = frame;
	this.srcX = this.curFrame * this.width; 
	this.srcY = this.curAnimation.row * this.height;
}

Sprite.prototype.setAnimation = function(animationName) {
	if (this.curAnimation.name == animationName) return;
	for (var a in this.animations){
		if (this.animations[a].name == animationName){
			this.curAnimation = this.animations[a];
			this.curFrame = 0;
			this.srcX = 0; 
			this.srcY = this.curAnimation.row * this.height;
		}
	}
	this.onAnimationEnd = null;
};

Sprite.prototype.startAnimation = function (animationName, onAnimationEnd) {
	if (animationName) this.setAnimation(animationName);
	this.animated = true;
	this.onAnimationEnd = onAnimationEnd;
};
Sprite.prototype.stopAnimation = function() {
	this.animated = false;
	this.curFrame = 0;
};

Sprite.prototype.draw = function(canvas, delta) {
	if (this.animated){
		if (delta + this.delta > 1000/this.animationSpeed){
			this.nextFrame();
		}
		this.delta = (delta + this.delta)%(1000/this.animationSpeed);
	}

	canvas.ctx.save();
	var signX = 1;
	var offsetX = this.origin.x;
	if (this.flipped){
		canvas.ctx.scale(-1, 1);
		signX = -1;
		offsetX = this.width - this.origin.x;
	}

	this.screenX = signX * (this.x - canvas.viewX - offsetX) * canvas.zoom;
	this.screenY = (this.y - canvas.viewY - this.origin.y) * canvas.zoom;
	this.screenWidth = signX * this.width * canvas.zoom;
	this.screenHeight = this.height * canvas.zoom;
	canvas.ctx.drawImage(this.img,
		this.srcX, this.srcY,
		this.width, this.height,
		this.screenX, this.screenY,
		this.screenWidth, this.screenHeight
	);

	canvas.ctx.restore();
};

module.exports = Sprite;