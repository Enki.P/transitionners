var $ = require("jquery");

function requireAll(r) { r.keys().forEach(r); }
requireAll(require.context("../../../assets/", true, /\.*$/));


var Assets = function () {
	this.assets = [
		{ url: "GUI/clock.png"},
		{ url: "GUI/clock_frame.png"},
		{ url: "GUI/close_button.png"},
		{ url: "GUI/coin.png"},
		{ url: "GUI/coinSprite.png"},
		{ url: "GUI/inventory_button.png"},
		{ url: "GUI/inventory_icon.png"},
		{ url: "GUI/shop_button.png"},
		{ url: "GUI/shop_icon.png"},
		{ url: "GUI/time_x1.png"},
		{ url: "GUI/time_x10.png"},
		{ url: "GUI/time_x100.png"},
		{ url: "items/plant/laitue/laitue_graine germée.png"},
		{ url: "items/plant/laitue/laitue_graine.png"},
		{ url: "items/plant/laitue/laitue_jeune_plante.png"},
		{ url: "items/plant/laitue/laitue_petite_pousse.png"},
		{ url: "items/plant/laitue/laitue_spriteSheet.png"},
		{ url: "items/tool/gants.png"},
		{ url: "items/tool/paillage.png"},
		{ url: "items/terrain/terrainSpriteSheet.png"},
		{ url: "items/terrain/field_concrete.png"},
		{ url: "items/terrain/field_empty.png"},
		{ url: "items/terrain/field_empty_wet.png"},
		{ url: "items/terrain/field_grass.png"},
		{ url: "items/terrain/field_grass_wet.png"},
		{ url: "items/terrain/field_plowed.png"},
		{ url: "items/terrain/field_plowed_wet.png"},
		{ url: "items/terrain/fond_balcon.png"},
		{ url: "items/terrain/fond_ferme.png"},
	]
}

Assets.prototype.get = function (url) {
	for (let a in this.assets) {
		if (this.assets[a].url == url) {
			return this.assets[a].image
		}
	}
}


Assets.prototype.loadAssets = function (onLoaded) {
	var _this = this;
	var allAssetsLoaded = true;
	var nbAssetsCharges = 0;
	var totalAssetsNb = _this.assets.length;

	var checkIfLoaded = function (id) {
		console.log("img " + nbAssetsCharges + "/" + totalAssetsNb + " (" + id + ")");
		if (nbAssetsCharges >= totalAssetsNb) {
			// _this.loadAssets(onLoaded);
		}
	}

	for (var i = 0; i < _this.assets.length; i++) {
		if (_this.assets[i].image != undefined) {
			nbAssetsCharges++;
			continue;
		}

		allAssetsLoaded = false;
		var downloadingImage = new Image();
		var url = _this.assets[i].url;

		downloadingImage.onload = function () {
			nbAssetsCharges++;
			_this.assets[this.id].image = this;
			checkIfLoaded(this.src);
		};
		downloadingImage.onerror = function (e) {
			//console.log(e);
			// console.log("no picture found : " + this.src);
			nbAssetsCharges++;
			// console.log("assets", app.model.assets);
			_this.assets[this.id].image = undefined;
			checkIfLoaded(this.src);
		};

		downloadingImage.id = i;
		downloadingImage.src = "../../../assets/" + url;
	}

	if (allAssetsLoaded) {
		// console.log("All Assets Loaded !");
		// console.log("assets", app.model.assets);
		onLoaded();
	}

	return _this.assets
}


module.exports = Assets;