var AppView = require("../views/appView.js");
var Terrain = require("../../terrain/controllers/terrain.js");
var Player = require("../../player/models/player.js");
var Sprite = require("../controllers/sprite.js");
var Controller = require("../controllers/controller.js");
var Canvas = require("../controllers/canvas.js");
var DateSystem = require("../controllers/dateSystem.js");
var $ = require("jquery");



var GameModel = function(init){
	if (init === undefined) init = true;
	if (init){
		this.init();
	}
}

GameModel.prototype.init = function() {
	var _this = this;

	this.canvas = new Canvas();
	this.player = new Player();
	this.terrain = new Terrain("ferme");
	this.date = new DateSystem();
	this.controller = new Controller(this);
	this.appView = new AppView(this);

	this.player.setPosition({x: this.terrain.terrainData.spawn[0], y: this.terrain.terrainData.spawn[1]}, true);
	var bgx = this.terrain.terrainData.backgroundSize[0];
	var bgy = this.terrain.terrainData.backgroundSize[1];
	this.canvas.maxGameHeight = bgy;
	this.canvas.maxGameWidth = bgx;

	this.canvas.cameraTarget = this.player;
	this.objectsToRender = [this.terrain, this.player];
};


module.exports = GameModel;