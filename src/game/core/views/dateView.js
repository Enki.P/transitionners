var $ = require("jquery");



var DateView = function(dateSystem){
	this.date = dateSystem;
}

DateView.prototype.renderDate = function() {
	this.renderYear();
	this.renderDayMonth();
	this.renderHour();
	this.renderSeason();
};

DateView.prototype.renderYear = function() {
	this._setTopInfoValue("year", this.date.date.getFullYear() - 2000);
};

DateView.prototype.renderDayMonth = function() {
	this._setTopInfoValue("dayMonth", this.date.date.toLocaleDateString("fr-FR", { month: 'long', day: 'numeric' }));
};


DateView.prototype.renderHour = function() {
	var _this = this;
	var ms = this.date.date.getTime()
	// this._setTopInfoValue("time", this.date.date.getHours() + "h");
	var deg = ms/(1000*60*60*24) * 360 - (360*11323);
	const $clock = $('#top-infos .info[info-type="hour"] #clock')
	$clock.css("transform", "rotate(-"+ deg +"Deg)");
};

DateView.prototype.renderSeason = function() {
	this._setTopInfoValue("season", this.date.getSeason());
};


DateView.prototype._setTopInfoValue = function(infoType, value) {
	$('#top-infos .info[info-type="'+ infoType +'"] .info-value').html(value);
};


module.exports = DateView;