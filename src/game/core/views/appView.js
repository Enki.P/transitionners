require("../../gui/css/gui.less");


var $ = require("jquery");
var gui = require("../../gui/controllers/gui.js");

var AppView = function(gameModel){
	this.gameModel = gameModel;
	this.currentTab = "";
	this.init();
}

AppView.prototype.init = function () {
	gui.addPanelBackground(".panel")
	this.renderCoins();

	var _this = this;
	$(".sidePanel-selecter").on("click", function (e) {
		var newTab = $(e.currentTarget).data("tab");
		if (_this.currentTab != newTab) {
			gui.displayTab(newTab);
			_this.currentTab = newTab;
		} else {
			gui.collapseSidePanel();
			$('.sidePanel-selecter.circle-btn').removeClass("selected")
			_this.currentTab = "";
		}
	});

	const dateSystem = this.gameModel.date
	$("#time-controls .panel").on("click", function () {
		$("#time-controls .panel").removeClass("selected")
		$(this).addClass("selected")
		dateSystem.setUserFactor(parseInt($(this).attr("time-factor")))
	})
};

AppView.prototype.renderCoins = function() {
	$('#top-infos .info[info-type="coins"] .info-value').html(
		this.gameModel.player.inventory.coins
	);
};



module.exports = AppView;