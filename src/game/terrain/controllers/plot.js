var Sprite = require("../../core/controllers/sprite.js");
var Animation = require("../../core/controllers/animation.js");

const plotModal = require("../../gui/controllers/plotModal.js");

var Plot = function(surface, plotX, plotY, x, y){
	this.surface = surface;
	this.state = "";
	if (this.surface == "field") this.state = "grass";
	this.wet = false;
	this.fertilizer = null;
	this.plotX = plotX;
	this.plotY = plotY;

	var animationsArray = [
		new Animation("concrete", 0, 1),
		new Animation('field_grass', 1, 4),
		new Animation('field_empty', 2, 4),
		new Animation('field_plowed', 3, 4)
	];
	this.sprite = new Sprite(300, 300, 100, 50, animationsArray, require("../../../assets/terrain/terrainSpriteSheet.png"));
	this.sprite.setAnimation(this.surface + "_" + this.state);

	this.sprite.x = x;
	this.sprite.y = y;

	this.spritesToRender = [];

	this.slots = [];
	for (var x = 0; x < 4; x++){
		this.slots[x] = [];
		for (var y = 0; y < 6; y++){
			this.slots[x][y] = {
				x: x,
				y: y,
				terrainX: 51 - 12*x + 8*y,
				terrainY: 7 + 6*x + 4*y
			};
		}
	}
}

Plot.prototype.getPlants = function(){
	var plants = [];
	for (var x in this.slots){
		var row = this.slots[x];
		for (var y in row){
			if (! row[y].plant) continue;
			plants.push(row[y].plant);
		}
	}
	return plants;
}

Plot.prototype.clearSlot = function (x, y) {
	this.slots[x][y].plant = undefined
	this.updateSpriteList();
}

Plot.prototype.setState = function(state){
	this.state = state;
	this.refreshSpriteImage();
}

Plot.prototype.setWet = function(wet){
	this.wet = wet;
	this.refreshSpriteImage();
}

Plot.prototype.setFertilizer = function(fertilizer){
	this.fertilizer = fertilizer;
	this.refreshSpriteImage();
}

Plot.prototype.refreshSpriteImage = function() {
	this.sprite.setAnimation(this.surface + "_" + this.state);
	this.sprite.setFrame(0);
	if (this.wet === true && this.fertilizer === null){
		this.sprite.setFrame(1);
		return;
	}
	if (this.wet === false && this.fertilizer === "mulch"){
		this.sprite.setFrame(2);
		console.log("not wet & mulch");
		return;
	}
	if (this.wet === true && this.fertilizer === "mulch"){
		this.sprite.setFrame(3);
		return;
	}
}

Plot.prototype.putPlantInGround = function(plant) {
	this.slots[0][0].plant = plant;
	plant.inventory = undefined;
	plant.slot = this.slots[0][0];
	this.spritesToRender.push(plant.sprite);
	plant.sprite.x = this.sprite.x + this.slots[0][0].terrainX;
	plant.sprite.y = this.sprite.y + this.slots[0][0].terrainY;
	plant.plantedInPlot = this;
};

Plot.prototype.updateSpriteList = function () {
	this.spritesToRender = []
	for (var x in this.slots) {
		var row = this.slots[x];
		for (var y in row) {
			if (!row[y].plant) continue;
			this.spritesToRender.push(row[y].plant.sprite);
		}
	}
}

Plot.prototype.showObservationModal = function () {
	plotModal.show(this)
}

module.exports = Plot;