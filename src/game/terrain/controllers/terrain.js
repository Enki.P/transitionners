require("../css/terrain.less");
var terrains = require("../data/terrains.json");
var Plot = require("./plot.js");
var Sprite = require("../../core/controllers/sprite.js");
var PF = require('pathfinding');

var $ = require("jquery");

var pfGridCaseWidth = 100/6;
var pfGridCaseHeight = 50/6;

var Terrain = function(terrainName){
	this.setTerrain(terrainName);
};

Terrain.prototype.setTerrain = function(terrainName) {
	var terrainData = {
		name: "test",
		shape: "normal",
		width: 1,
		height: 1,
		plotOffset: [382, 177],
		src: "fond_balcon.png",
		spawn: [560,294],
		boundaries: [
			[433,227],
			[633,327],
			[532,377],
			[332,277]
		],
		backgroundSize : [1000,500],
		surface: "concrete"
	};
	for (var t in terrains){
		if (terrains[t].name == terrainName){
			$.extend(terrainData,terrains[t]);
		}
	}
	this.terrainData = terrainData;
	this.plotsArray = [];
	var pfGridMatrix = [[]];
	var minY, maxY, minX, maxX;
	// construit la matrice de pathfinding
	// true = bloqué, false = libre
	for (var x=0; x<terrainData.width; x++){
		for (var y=0; y<terrainData.height; y++){
			// screenX, screenY : coordonnées sur l'écran du coin haut-gauche du plot courant
			var screenX, screenY;

			switch (terrainData.shape){
				case "normal":
					screenX = terrainData.plotOffset[0] + (x-y) * 50;
					screenY = terrainData.plotOffset[1] + (x+y) * 25;
					break;
				case "horizontal":
					screenX = terrainData.plotOffset[0] + x * 100 + 50*(y%2);
					screenY = terrainData.plotOffset[1] + y * 25;
					
			}
			var deltaXLeft = 0, deltaXRight = 0, deltaYTop = 0, deltaYBottom = 0,
				plotMatX, plotMatY;

			// initialisation de la matrice (création d'un champ)
			if (minX == null) {
				this._addRowsToMatrix(pfGridMatrix, 5, true);
				this._addColsToMatrix(pfGridMatrix, 6, true);
				plotMatX = 0;
				plotMatY = 0;
			// agrandissement de la matrice si nécessaire (création d'un champ qui dépasse)
			} else {
				deltaXLeft = parseInt((minX - screenX)/pfGridCaseWidth) ;
				plotMatX = -deltaXLeft;
				if (deltaXLeft > 0){
					this._addColsToMatrix(pfGridMatrix, deltaXLeft, true, true);
					plotMatX = 0;
				}
				deltaXRight = parseInt((screenX - maxX)/pfGridCaseWidth) + 6;
				if (deltaXRight > 0){
					this._addColsToMatrix(pfGridMatrix, deltaXRight, true);
				}
				deltaYTop = parseInt((minY - screenY)/pfGridCaseHeight) ;
				plotMatY = -deltaYTop;
				if (deltaYTop > 0){
					this._addRowsToMatrix(pfGridMatrix, deltaYTop, true, true);
					plotMatY = 0;
				}
				deltaYBottom = parseInt((screenY - maxY)/pfGridCaseHeight) + 6;
				if (deltaYBottom > 0){
					this._addRowsToMatrix(pfGridMatrix, deltaYBottom, true);
				}
			}
			// mise à jour, remplissage de ma matrice
			for (var i=0; i<6; i++){
				for (var j=0; j<6; j++){
					// if ((i > 2 && i < 5) || (i > 0 && i < 7 && j > 0 && j < 3)){
					if (Math.abs(i-2.5) + Math.abs(j-2.5) <= 3){
						pfGridMatrix[plotMatY + j][plotMatX + i] = false;
					}
				}
			}
			
			// // debug
			// console.log("________________________");
			// for(var x0 in pfGridMatrix){
			// 	var dispArray = []
			// 	for (var y0 in pfGridMatrix[x0]){
			// 		dispArray.push(pfGridMatrix[x0][y0] ? "1" : 0);
			// 	}
			// 	console.log(dispArray.join(" - "));
			// }

			// mise à jour des coordonnées min et max de la matrice
			if (screenY < minY || minY == null) minY = screenY;
			if (screenY+50 > maxY || maxY == null) maxY = screenY+50;
			if (screenX < minX || minX == null) minX = screenX;
			if (screenX+100 > maxX || maxX == null) maxX = screenX+100;

			// ajout du champ au terrain
			this.plotsArray.push(new Plot(terrainData.surface, x, y, screenX, screenY));
		}
	}

	// redéfinition de la taille du canvas
	var bgx = terrainData.backgroundSize[0];
	var bgy = terrainData.backgroundSize[1];
	// affichage de l'image de fond
	var path = require("../../../assets/terrain/" + terrainData.src);
	this.backgroundSprite = new Sprite(bgx, bgy, bgx, bgy, false, path);
	this.backgroundSprite.zIndex = -1;

	// tableau des sprites à dessiner
	this.spritesToRender = [this.backgroundSprite];
	this.objectsToRender = [];
	for (var p in this.plotsArray){
		this.plotsArray[p].sprite.zIndex = 0;
		this.spritesToRender.push(this.plotsArray[p].sprite);
		this.objectsToRender.push(this.plotsArray[p]);
	}

	// initialisation du pathFinder
	this.gridStartCoords = {x: minX, y: minY};
	this.pfGrid = new PF.Grid(pfGridMatrix);
	this.pfFinder = new PF.BestFirstFinder({allowDiagonal: true, dontCrossCorners: true});
};

Terrain.prototype._addRowsToMatrix = function(matrix, rows, value, addOnTop) {
	if (addOnTop === undefined) addOnTop = false;
	// console.log("add " + rows + " rows " + (addOnTop ? "on top" : ""));
	var lenght = matrix[0].length;
	for (var i=0; i<rows; i++){
		var newRow = [];
		for (var j=0; j<lenght; j++){
			newRow.push(value);
		}
		if (addOnTop) matrix.unshift(newRow);
		else matrix.push(newRow);
	}
};
Terrain.prototype._addColsToMatrix = function(matrix, cols, value, addOnFront) {
	if (addOnFront === undefined) addOnFront = false;
	// console.log("add " + cols + " cols " + (addOnFront ? "on front" : ""));
	for (var i=0; i<cols; i++){
		for (var j=0; j<matrix.length; j++){
			if (addOnFront) matrix[j].unshift(value);
			else matrix[j].push(value);
		}
	}
};

Terrain.prototype.getGridCoordsFromGameCoords = function(coordsObj){
	var gameX = coordsObj.x - this.gridStartCoords.x;
	var gameY = coordsObj.y - this.gridStartCoords.y;
	return {
		x: Math.floor(gameX/pfGridCaseWidth),
		y: Math.floor(gameY/pfGridCaseHeight)
	}
};

Terrain.prototype.getPath = function(deplacementData) {
	var grid = this.pfGrid.clone();
	var fromX = deplacementData.from.grid.x;
	var fromY = deplacementData.from.grid.y;
	var toX = deplacementData.to.grid.x;
	var toY = deplacementData.to.grid.y;
	var path = this.pfFinder.findPath(fromX, fromY, toX, toY, grid);
	path = PF.Util.compressPath(path);

	path[0] = [deplacementData.from.exact.x, deplacementData.from.exact.y];
	path[path.length-1] = [deplacementData.to.exact.x, deplacementData.to.exact.y];
	for (var i=1; i<path.length-1; i++){
		var gridCoords = [path[i][0], path[i][1]];
		path[i] = [
			gridCoords[0] * pfGridCaseWidth + 6.25 + this.gridStartCoords.x,
			gridCoords[1] * pfGridCaseHeight + 6.25 + this.gridStartCoords.y,
		]
	}
	return path;
};

Terrain.prototype.getPlotFromClickCoords = function(coordsObj){
	var terrainData = this.terrainData;
	var plotX, plotY;
	var x = coordsObj.x,
		y = coordsObj.y,
		ox = terrainData.plotOffset[0],
		oy = terrainData.plotOffset[1];
	switch (terrainData.shape){
		case "normal":
			plotY = Math.floor(( y-oy - (x-ox)/2 )/50 + 0.5);
			plotX = Math.floor(( y-oy + (x-ox)/2 )/50 - 0.5);
			break;
		case "horizontal":
			var col = Math.floor(( y-oy - (x-ox)/2 )/50 + 0.5);
			var line = Math.floor(( y-oy + (x-ox)/2 )/50 - 0.5);
			plotX = (line - col - (line - col)%2)/2;
			plotY = line + col;
	}

	return this._getPlotFromPlotCoords({x: plotX, y: plotY});
}

Terrain.prototype._getPlotFromPlotCoords = function(coordsObj) {
	var plotsArray = this.plotsArray;
	var coordsId = coordsObj.x * 10000 + coordsObj.y;
	function dicoFunction(pos){
		var coordsId2 = plotsArray[pos].plotX * 10000 + plotsArray[pos].plotY;
		return coordsId2 - coordsId;
	}
	
	var minPos = 0;
	var maxPos = plotsArray.length - 1;
	
	if (coordsObj.x < plotsArray[minPos].plotX || coordsObj.x > plotsArray[maxPos].plotX
		|| coordsObj.y < plotsArray[minPos].plotY || coordsObj.y > plotsArray[maxPos].plotY) return false;

	while ((maxPos - minPos) >= 1){
		var m = Math.floor((minPos + maxPos) / 2);
		if (dicoFunction(minPos)*dicoFunction(m) == 0) return plotsArray[m];
		if (dicoFunction(minPos)*dicoFunction(m) < 0){
			maxPos = m;
		} else {
			minPos = m;
		}
	}
};




module.exports = Terrain;