require("../css/gui.less");

const $ = require("jquery");
require("jquery-ui/ui/widgets/draggable");
require("jquery-ui/ui/widgets/droppable");

const itemModal = {
	$modal: $('#item-modal'),

	setItem: function (item, $item) {
		const $modal = this.$modal;
		$modal.data("item", item);
		$modal.data("item-ui", $item);
		$modal.find(".item-icon").attr("src", item.getIcon());
		$modal.find(".item-quantity label").text(item.quantity);
		$modal.find(".item-details").hide();
		$modal.find(".item-details." + item.itemType).show();

		const descriptionFields = item.getDescriptionFields();
		$modal.find(".item-name").text(descriptionFields.name);

		if (item.itemType == "plant") {
			$modal.find(".plant-seeding-season label").text(descriptionFields.seedingStart + " à " + descriptionFields.seedingEnd);
			$modal.find(".plant-repiquage-step label").text(descriptionFields.repiquage);
			$modal.find(".plant-harvest-season label").text(descriptionFields.harvest);
			$modal.find(".plant-description label").text(descriptionFields.description);
		}
	},

	show: function (item, $item, btnTxt, btnAction, onhide) {
		var isInStockage = true;
		if ($item.parents('#hotbar').length > 0) {
			isInStockage = false;
		}
		const $modal = this.$modal;

		this.setItem(item, $item)

		if (item.default == true) {
			$modal.find(".move-item-btn").addClass("disabled");
		} else {
			$modal.find(".move-item-btn").removeClass("disabled");
		}

		if (btnTxt && typeof btnAction == 'function') {
			$modal.find(".move-item-btn .text-content").text(isInStockage ? "Prendre" : "Stocker");
			$modal.find(".move-item-btn").off().on("click", btnAction);
		}
		
		const _this = this
		const hide = function () {
			_this.hide();
			if (typeof onhide === "function") {
				onhide();
			}
		}
		$modal.find('.modal-close').on("click", hide);
		$modal.find('.modal-background').on("click", hide);

		$modal.removeClass("hidden");
	},

	hide: function () {
		const $modal = this.$modal;
		$modal.addClass("hidden");
		$modal.data("item-ui").removeClass("selected");
		$modal.find('.modal-background').off("click");
	}
}


module.exports = itemModal;