require("../css/selector.less");


var $ = require("jquery");


var Selector = function(options) {
	this.data = options.data;
	this.$container = options.container;
	this.expanded = false;
	this.onChange = options.onChange;

	this.init();
}

Selector.prototype.init = function() {
	var $c = this.$container;

	$c.addClass("selector");
	$html = [];
	var defaultData = this._getDefaultData();
	$html.push(	'<div class="selector-selected">');
	$html.push(		'<div class="selector-option" data-value="'+defaultData.value+'">'+defaultData.text+'</div>');
	$html.push(		'<div class="selector-arrow arrow-down"></div>');
	$html.push(	'</div>');
	$html.push(	'<div class="selector-options">');
	for (var d in this.data){
		var data = this.data[d];
		$html.push(	'<div class="selector-option" data-value="'+data.value+'">'+data.text+'</div>');
	}
	$html.push(	'</div>');
	$c.empty().append($html.join(""));
	this.defaultWidth = $c.find(".selector-options .selector-option").innerWidth()-105;
	$c.find('.selector-options').hide();

	this.select(defaultData.value);

	var _this = this;
	$c.on("click", function(){
		_this.toggle();
	});

	$c.find('.selector-option').on("click", function(){
		_this.select($(this).data("value"));
	});
};


Selector.prototype._getDataFromValue = function(value){
	for (var d in this.data){
		var data = this.data[d];
		if (data.value == value){
			return data;
		}
	}
}

Selector.prototype._getDefaultData = function(){
	for (var d in this.data){
		var data = this.data[d];
		if (data.default === true){
			return data;
		}
	}
}

Selector.prototype._setdefaultWidth = function() {
	var $c = this.$container;
	$c.find(".selector-selected .selector-option").css("width", this.defaultWidth);
};

Selector.prototype.slideDown = function(){
	this.$container.find('.selector-arrow ').removeClass("arrow-down").addClass("arrow-up");
	this.$container.find('.selector-options').slideDown();
	this.expanded = true;
}

Selector.prototype.slideUp = function(){
	this.$container.find('.selector-arrow ').removeClass("arrow-up").addClass("arrow-down");
	this.$container.find('.selector-options').slideUp();
	this.expanded = false;
}

Selector.prototype.toggle = function(){
	if (this.expanded){
		this.slideUp();
	} else {
		this.slideDown();
	}
}

Selector.prototype.getVal = function(){
	return this.$container.find('.selector-selected .selector-option').data("value");
}

Selector.prototype.select = function(value) {
	var $c = this.$container;

	$c.find(".selector-selected .selector-option").remove();
	$c.find('.selector-options .selector-option').removeClass("selected").show();
	var $selectedOption = $c.find('.selector-options .selector-option[data-value="'+value+'"]');
	$selectedOption.clone(true).prependTo('.selector-selected');
	this._setdefaultWidth();
	$selectedOption.addClass("selected");

	if (typeof this.onChange == "function"){
		this.onChange();
	}
};


module.exports = Selector;