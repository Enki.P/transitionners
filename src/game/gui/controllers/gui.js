require("../css/gui.less");

const $ = require("jquery");
require( "jquery-ui/ui/widgets/draggable" );
require("jquery-ui/ui/widgets/droppable");

const itemModal = require("./itemModal.js");

const gui = {
	addPanelBackground: function ($container) {
		const panelBg = require('../templates/panelBg.html')
		this._addBackground($container, panelBg)
	},

	_addBackground: function ($container, bg) {
		if (typeof $container === "string") {
			const selector = $container
			$(selector).each(function () {
				$(this).prepend(bg)
			});
			return
		}
		$container.prepend(bg);
	},

	blurCanvas: function() {
		$("#GUI-bg").css("pointer-events", 'all');
	},

	focusCanvas: function(){
		$("#GUI-bg").css("pointer-events", '');
	},

	displayTab: function (tabName) {
		// hide all content
		$('.sidePanel-content').hide();
		// show tab content
		$('.sidePanel-content.' + tabName).show();
		// expand side panel
		$("#sidePanel").removeClass("collapsed");
		// hide modal
		$('.modal').addClass("hidden");
		// set all selecters unselected, except the selected one
		$('.sidePanel-selecter.circle-btn').removeClass("selected")
		$('.sidePanel-selecter.circle-btn.' + tabName).addClass("selected")
	},

	collapseSidePanel: function() {
		$("#sidePanel").addClass("collapsed");
		$("#GUI-bg").css("pointer-events", 'none');
		$(".hands-item-selector").slideUp();
		$(".hands-item-selector").removeClass("visible");
		$('.modal').addClass("hidden");
	},

	showItemModal: function(item, $item, btnTxt, btnAction) {
		this.blurCanvas();
		itemModal.show(item, $item, btnTxt, btnAction, this.focusCanvas)
	},

	hideItemModal: function (item, $item, btnTxt, btnAction) {
		itemModal.hide()
	},

	showActionsModal: function (clickedPlot, actions, actionFunction, onClose) {
		const modalCoords = {
			x: clickedPlot.sprite.screenX + 0.5 * clickedPlot.sprite.screenWidth,
			y: clickedPlot.sprite.screenY + 0.5 * clickedPlot.sprite.screenHeight
		};
		
		let modalHtml = '<div class="actions-modal modal-container hidden">';
				modalHtml += '<div class="modal" style="top: ' + modalCoords.y + 'px; left: ' + modalCoords.x +'px">';
					modalHtml += '<div class="modal-content">';
						modalHtml += '<div class="modal-buttons">';
							// modalHtml += '<div class="btn panel panel-small"><div class="text-content">Observer</div></div>';
						modalHtml +='</div>';
					modalHtml += '</div>';
				modalHtml += '</div>';
			modalHtml += '</div>';

		const $modal = $(modalHtml);
		$('#GUI').append($modal);

		const $modalButtons = $modal.find('.modal-buttons');

		$modalButtons.find(".panel").on("click", function(){
			console.log("TO DO : slot information modal");
		});

		for (let a in actions){
			const text = actions[a].name;
			const $button = $('<div class="btn panel panel-small"><div class="text-content">'+text+'</div></div>');
			$button.on("click", function(){
				actionFunction(actions[a].action);
			});

			$modalButtons.append($button);
		}

		const $cancel = $('<div class="circle-btn small btn-close"></div>');
		$modalButtons.append($cancel);

		$modalButtons.find('.panel,.btn-close').on("click", function(){
			if (typeof onClose === "function") onClose();
		});
		

		this.addPanelBackground(".actions-modal .panel");

		setTimeout(function() {$modal.removeClass("hidden");}, 1); 
	},

	hideActionsModals: function(){
		const $modal = $('.actions-modal');
		$modal.addClass("hidden");
		setTimeout(function() {
			$modal.remove();
		}, 200);
	},
}


module.exports = gui;