require("../css/gui.less");

const $ = require("jquery");
require("jquery-ui/ui/widgets/draggable");
require("jquery-ui/ui/widgets/droppable");

const plotModal = {
	$modal: $('#plot-modal'),

	setPlot: function (plot) {
		const $modal = this.$modal;
		const $plotZoom = $modal.find(".plot-zoom .panel-content")
		$plotZoom.find(".plot-img").remove()
		const $plotImg = $('<div class="plot-img"/>')
		$plotZoom.append($plotImg)
		$plotImg.css("background-image", "url(" + plot.sprite.img.src + ")")
		$plotImg.css("background-position-x", (100 * plot.sprite.curFrame) + "%")
		$plotImg.css("background-position-y", (-100 * plot.sprite.curAnimation.row) + "%")
		
		const $plants = $plotZoom.find(".plants")
		$plants.empty()
		for (var x in plot.slots) {
			for (var y in plot.slots[x]) {
				if (plot.slots[x][y].plant) {
					const slot = plot.slots[x][y];
					const plant = slot.plant;
					let $plant = $('<div class="plant"></div>')
					$plants.append($plant)
					let $plantImg = $('<div class="plant-img"></div>')
					$plant.append($plantImg)
					$plantImg.css("background-image", "url(" + plant.sprite.img.src + ")")
					const pxRatio = $plotImg.width() / plot.sprite.width
					const plantWidth = pxRatio * plant.sprite.width
					const plantHeight = plantWidth * plant.sprite.height / plant.sprite.width
					$plantImg.css("background-position-x", (plantWidth * plant.sprite.curFrame) + "px")
					$plantImg.css("background-position-y", (-plantHeight * (plant.sprite.curAnimation.row)) + "px")
					$plantImg.css("width", plantWidth + "px")
					$plantImg.css("background-size", plantWidth + "px")
					$plantImg.css("padding-top", plantHeight + "px")
					$plant.css("left", pxRatio * (slot.terrainX - plant.spriteInfos.origin.x) + "px");
					$plant.css("top", pxRatio * (slot.terrainY - plant.spriteInfos.origin.y) + "px");
					let $loupe = $('<img class="loupe-img" src="./assets/items/tool/loupe.png"/>')
					$plant.append($loupe)
					$loupe.css("left", pxRatio * plant.spriteInfos.origin.x + "px");
					$loupe.css("top", pxRatio * plant.spriteInfos.origin.y + "px");

					const _this = this
					$loupe.on("click", function () {
						_this.setPlant(plant)
					})
				}
			}
		}
	},

	setPlant: function (plant) {
		const $modal = this.$modal;
		const $slotDetails = $modal.find(".slot-details")
		$slotDetails.removeClass("hidden")
		$slotDetails.find(".item-icon").attr("src", plant.getIcon())
		const descriptionFields = plant.getDescriptionFields();
		$slotDetails.find(".item-name").text(descriptionFields.name)

	},

	show: function (plot) {
		const $modal = this.$modal;
		const _this = this

		this.setPlot(plot)

		$modal.find('.modal-close').on("click", _this.hide);
		$modal.find('.modal-background').on("click", _this.hide);

		$modal.removeClass("hidden");
	},

	hide: function () {
		const $modal = $('#plot-modal');
		$modal.addClass("hidden");
		$modal.find(".slot-details").addClass("hidden")
		$modal.find('.modal-background').off("click");
	}
}


module.exports = plotModal;