const path = require('path'),
	CleanWebpackPlugin = require('clean-webpack-plugin'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	
	// absolute path for project root
	context: path.resolve(__dirname, 'src'),

	mode: "development",

	entry: {
		// relative path declaration
		app: './index.js'
	},

	output: {
		// absolute path declaration
		path: path.resolve(__dirname, 'dist'),
		filename: './all.js'
	},

	module: {
		rules: [
			//html-loader
			{ test: /\.html$/, use: ['html-loader'] },

			//file-loader(for images)
			{
				test: /\.(jpg|png|gif|svg)$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[path][name].[ext]'
					}
				}]
			},
			//file-loader(for fonts)
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: ['file-loader']
			},

			//less-loader
			{
				test: /\.(less|css)$/,
				use: [{
					loader: MiniCssExtractPlugin.loader,
					options: {
						// you can specify a publicPath here
						// by default it use publicPath in webpackOptions.output
						publicPath: './'
					}
				}, {
					loader: "css-loader" // translates CSS into CommonJS
				}, {
					loader: "less-loader" // compiles Less to CSS
				}]
			}
		]
	},

	plugins: [
		// cleaning up only 'dist' folder
		new CleanWebpackPlugin(['dist']),
		//html-webpack-plugin instantiation
		new HtmlWebpackPlugin({
			template: 'index.html'
		}),

		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: "[name].css",
			chunkFilename: "[id].css"
		})
	],

	devServer: {
		// static files served from here
		contentBase: path.resolve(__dirname, "./dist/assets/media"),
		compress: true,
		// open app in localhost:2000
		port: 2000,
		open: true
	},

	devtool: 'inline-source-map'

};